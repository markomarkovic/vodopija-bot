FROM node:8.11.1

ADD . /app

WORKDIR /app
RUN yarn install --pure-lockfile --no-cache --production

EXPOSE 3000

ENTRYPOINT ["node", "."]
