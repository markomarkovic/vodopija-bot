const fbTemplate = require('claudia-bot-builder/lib/facebook/format-message')
const {send} = require('./common')

const INTENT_DRINK_START = 'intent_drink_start'
const INTENT_DRINK_YES = 'intent_drink_yes'
const INTENT_DRINK_NO = 'intent_drink_no'
const INTENT_DRINK_FAVORITE = 'intent_drink_favorite'

const run = (msg, recipient) => {
  send(recipient.id, [
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(500).get(),
    new fbTemplate.Text(`
    So, you'd like to drink?
    `)
      .addQuickReply('YES!', INTENT_DRINK_YES)
      .addQuickReply('No, not particulary', INTENT_DRINK_NO)
      .get(),
  ])
}

const runYes = (msg, recipient) => {
  send(recipient.id, [
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(500).get(),
    new fbTemplate.Text(`
      YEAH, me too.
      `).get(),
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(1000).get(),
    new fbTemplate.Text(`
      What's your favorite drink?
      `)
      .addQuickReply('Absinthe', INTENT_DRINK_FAVORITE)
      .addQuickReply('Beer', INTENT_DRINK_FAVORITE)
      .addQuickReply('Brandy', INTENT_DRINK_FAVORITE)
      .addQuickReply('Cachaça', INTENT_DRINK_FAVORITE)
      .addQuickReply('Gin', INTENT_DRINK_FAVORITE)
      .addQuickReply('Ouzo', INTENT_DRINK_FAVORITE)
      .addQuickReply('Rum', INTENT_DRINK_FAVORITE)
      .addQuickReply('Sake', INTENT_DRINK_FAVORITE)
      .get(),
  ])
}

const runFavorite = (msg, recipient) => {
  send(recipient.id, [
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(500).get(),
    new fbTemplate.Text(
      `Hmmm... I don't like ${
        msg.originalRequest.message.text
      }, but I like water!`
    ).get(),
  ])
}

const runNo = (msg, recipient) => {
  send(recipient.id, [
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(500).get(),
    new fbTemplate.Text(`
    That's a shame, but do drink water!
    `).get(),
  ])
}

module.exports = [
  {
    keywords: [INTENT_DRINK_START, 'drink', 'dring', 'water', 'gimmme'],
    run,
  },
  {
    keywords: [INTENT_DRINK_YES],
    run: runYes,
  },
  {
    keywords: [INTENT_DRINK_NO],
    run: runNo,
  },
  {
    keywords: [INTENT_DRINK_FAVORITE],
    run: runFavorite,
  },
]
