const huh = require('huh')
const fbTemplate = require('claudia-bot-builder/lib/facebook/format-message')
const {send} = require('./common')

const uncapitalize = str => str.charAt(0).toLowerCase() + str.slice(1)

const run = (msg, recipient) => {
  send(recipient.id, [
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Text(
      `Sorry, ${
        recipient.first_name
      }. I didn't understand that, probably because of ${uncapitalize(
        huh.get()
      )}`
    ).get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(1000).get(),
    new fbTemplate.Text(
      `I'll improve in time, don't worry. Meanwhile, you can type 'start' to start the show over.`
    ).get(),
  ])
}

module.exports = [
  {
    id: 'default',
    keywords: [],
    run,
  },
]
