const fbReply = require('claudia-bot-builder/lib/facebook/reply')

const send = (recipientId, message) =>
  fbReply(recipientId, message, process.env.FB_ACCESS_TOKEN)

const noop = () => {}

module.exports = {
  send,
  noop,
}
