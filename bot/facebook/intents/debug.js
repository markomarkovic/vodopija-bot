const flatten = require('flatten')
const fbTemplate = require('claudia-bot-builder/lib/facebook/format-message')
const {send} = require('./common')

const INTENT_DEBUG_START = 'intent_debug_start'

const run = (msg, recipient, data) => {
  const text = flatten(data)
    .map(d => JSON.stringify(d, null, 2))
    .join(', ')
  send(recipient.id, new fbTemplate.Text(`DEBUG: \`${text}\``).get())
}

module.exports = [
  {
    id: 'debug',
    keywords: [INTENT_DEBUG_START, 'debug'],
    run,
  },
]
