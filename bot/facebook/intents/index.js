const flatten = require('flatten')

const load = intents => flatten(intents.map(name => require(`./${name}`)))

const intents = load(['default', 'start', 'drink', 'debug'])

const normalize = str => str.toLowerCase()

const matchIntent = str =>
  intents
    .map(i => (i.keywords.indexOf(normalize(str)) > -1 ? i.run : false))
    .find(f => f !== false)

const get = msg => {
  const defaultIntent = intents.find(i => i.id === 'default').run
  let intent = matchIntent(msg.text)

  if (!intent) {
    if (msg.quick_reply) {
      intent = matchIntent(msg.quick_reply.payload)
    }
  }

  return intent || defaultIntent
}

const run = (msg, recipient) => {
  const debugIntent = intents.find(i => i.id === 'debug').run
  const intent = get(msg)

  if (intent === debugIntent) {
    return debugIntent(msg, recipient, intents)
  }

  return intent(msg, recipient)
}

module.exports = {
  run,
}
