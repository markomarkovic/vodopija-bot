const fbTemplate = require('claudia-bot-builder/lib/facebook/format-message')
const {send, noop} = require('./common')

const INTENT_START_START = 'intent_start_start'
const INTENT_START_ABOUT = 'intent_start_about'
const INTENT_START_CHANGE_ALERTS = 'intent_change_alert'

const run = (msg, recipient) => {
  send(recipient.id, [
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(500).get(),
    new fbTemplate.Text(`
      This is your menu. You can reach it by writing Menu, or Help, or Start
      `)
      .addQuickReply('About vodopija-bot', INTENT_START_ABOUT)
      .addQuickReply('Change alerts', INTENT_START_CHANGE_ALERTS)
      .get(),
  ])
}

const runAbout = (msg, recipient) => {
  send(recipient.id, [
    new fbTemplate.ChatAction('mark_seen').get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Text(`Thanks for asking 🙂`).get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(500).get(),
    new fbTemplate.Text(
      `vodolija-bot was created by Marko Marković as a test for SPARTANS AI LTD.`
    ).get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(1000).get(),
    new fbTemplate.Text(
      `Its goal is to help you drink more water for a healthier life.`
    ).get(),
    new fbTemplate.ChatAction('typing_on').get(),
    new fbTemplate.Pause(1000).get(),
    new fbTemplate.Text(`
It features:
☑  Daily water reminders
☑  Personalized AI recommendations
☑  Number of cups of water drank this week
☑  Tips about water drinking
`)
      .addQuickReply('Back', INTENT_START_START)
      .get(),
  ])
}

module.exports = [
  {
    keywords: [INTENT_START_START, 'start', 'menu', 'help'],
    run,
  },
  {
    keywords: [INTENT_START_ABOUT],
    run: runAbout,
  },
  {
    keywords: [INTENT_START_CHANGE_ALERTS],
    run: noop,
  },
]
