# vodopija-bot

> A bot to remind you to drink plenty of water.


## Development

### 1. Install dependencies

    $ npx yarn

### 2. Set environment variables

#### 2.1.  Generate Facebook Verify Token

    $ export FB_VERIFY_TOKEN=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 32 | head -n 1)
    $ echo Facebook Access Token: $FB_VERIFY_TOKEN

#### 2.2 Set [App Secret][1]

    $ export FB_APP_SECRET=''

#### 2.3. Set [Facebook Access Token][2]

    $ export FB_ACCESS_TOKEN=''

#### 2.4 Set local dev server port

    $ export PORT=3000

### 3. Run the local dev server

    $ npx yarn run dev

### 4. Point ngrok to that local dev server

    $ npx ngrok http $PORT -region eu

### 5. Point the [Facebook webhook][3] to the ngrok tunnel

    https://SOME_RANDOM_ID.ngrok.io/api/v1/facebook/

### 6. Start chatting with your bot

#### 6.1 [Monitor your requests][4]

#### Upgrade package versions

    $ npx yarn upgrade-interactive --latest


## Production

    $ docker-compose up -d


[1]: https://developers.facebook.com/apps/YOUR_APP_ID/settings/basic/
[2]: https://developers.facebook.com/apps/YOUR_APP_ID/messenger/settings/
[3]: https://developers.facebook.com/apps/YOUR_APP_ID/webhooks/
[4]: http://localhost:4040/inspect/http
