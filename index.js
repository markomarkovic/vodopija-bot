const micro = require('micro')
const {router} = require('microrouter')

const fbBot = require('./bot/facebook')

const bots = [fbBot, (req, res) => micro.send(res, 404, 'Not found.')]

const server = micro(router(...bots))

const port = process.env.PORT || 3000
server.listen(port)
console.info(`Listening on port ${port}`)

module.exports = server
